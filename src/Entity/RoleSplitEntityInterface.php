<?php

namespace Drupal\config_role_split\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Role Split entities.
 */
interface RoleSplitEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
